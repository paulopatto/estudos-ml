from sklearn.naive_bayes import MultinomialNB

# CARACTERISTICAS
#[ eh_gordinho?, tem_perna_curta?, late? ]


pig1 = [1, 1, 0]
pig2 = [1, 1, 0]
pig3 = [1, 1, 0]

dog1 = [1, 1, 1]
dog2 = [0, 1, 1]
dog3 = [0, 1, 1]

database = [
    pig1, pig2, pig3,
    dog1, dog2, dog3
]

marcados = [
    1, 1, 1,
    -1, -1, -1
]

misterioso1 = [1, 1, 1]
misterioso2 = [1, 0, 0]
misterioso3 = [1, 0, 0]

teste = [misterioso1, misterioso2, misterioso3]


modelo = MultinomialNB()
modelo.fit(database, marcados)
print(modelo.predict(teste))
