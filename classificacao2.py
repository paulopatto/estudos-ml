from sklearn.naive_bayes import MultinomialNB

# CARACTERISTICAS
#[ eh_gordinho?, tem_perna_curta?, late? ]

GORDO       = 1
MAGRO       = 0
PERNA_CURTA = 1
PERNA_LONGA = 0
LATE        = 1
RONCA       = 0

PORCO    = 'PIG'
CACHORRO = 'DOG'

pig1 = [GORDO, PERNA_CURTA, RONCA]
pig2 = [GORDO, PERNA_CURTA, RONCA]
pig3 = [GORDO, PERNA_CURTA, RONCA]

dog1 = [GORDO, PERNA_CURTA, LATE]
dog2 = [MAGRO, PERNA_CURTA, LATE]
dog3 = [MAGRO, PERNA_CURTA, LATE]

database = [
    pig1, pig2, pig3,
    dog1, dog2, dog3
]

marcados = [
    PORCO, PORCO, PORCO,
    CACHORRO, CACHORRO, CACHORRO
]

esperados = [CACHORRO, PORCO, CACHORRO]

misterioso1 = [GORDO, PERNA_CURTA, LATE]
misterioso2 = [GORDO, PERNA_LONGA, RONCA]
misterioso3 = [MAGRO, PERNA_LONGA, LATE]

teste = [misterioso1, misterioso2, misterioso3]


modelo = MultinomialNB()
modelo.fit(database, marcados)
predicted = modelo.predict(teste)

print(predicted)



