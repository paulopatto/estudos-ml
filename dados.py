import csv
from sklearn.naive_bayes import MultinomialNB

def load_visits():
    data   = []
    labels = []
    with open('./dados/acesso.csv', newline='') as file:
        content = csv.reader(file)
        next(content, None) # Skip header
        for visit_home, visit_info, visit_contact, buy in content:
            data.append([
                int(visit_home),
                int(visit_info),
                int(visit_contact)
            ])
            labels.append(int(buy))
    return data, labels

def perform():
    model = MultinomialNB()
    data, labels = load_visits()
    model.fit(data, labels)
    samples = [
        [1, 0, 1],
        [0, 1, 0],
        [1, 1, 0]
    ]
    print(model.predict(samples))



perform()
